# Charlie K - 2016
# 02 - Playing with Samples

puts "Charlie K Was Here"

# Finding Paths

# Bass
bass1 = "d:/_samples/ghost_1/Basslines/"
bass2 = "d:/_samples/ghost_4/Basslines/"

# FX
fx1 = "d:/_samples/ghost_1/FX/"
fx2 = "d:/_samples/ghost_2/FX/"

# Drum Loops
loop1 = "d:/_samples/ghost_1/Loops/"
loop2 = "d:/_samples/ghost_preview/Drumloops/"
loop3 = "d:/_samples/_self/"

start_time = Time.new
loop_end_time = Time.new
duration = loop_end_time - start_time


in_thread delay: 0 do
  while Time.new - start_time < 24 do
      sample fx1, 11, release: 0.3, pitch: rrand(0.5, 1), amp: 0.5
    sleep 16
    end
end

in_thread delay: 4 do
 while Time.new - start_time < 40 do
        sample loop1, 1, amp: 2
        sleep 6
    end
end

in_thread delay: 8 do
 while Time.new - start_time < 50 do
          sample fx2, 2, amp: 2, release: 0.8, attack: 0.2, pitch: rrand(-0.3, 0.5)
          sleep rrand(1, 9)
    end
end

in_thread delay: 12 do
 while Time.new - start_time < 50 do
            sample bass1, 10, amp: 2, release: 2, attack: 0.5, pitch: rrand(-0.3, 0.5)
            sleep 2
  end
end

in_thread delay: 50 do
 while Time.new - start_time < 92 do
            sample loop2, 4, amp: 2, release: 2, attack: 0.5
            sleep 2
  end
end

in_thread delay: 54 do
 while Time.new - start_time < 92 do
            sample bass2, 3, amp: 1, release: 2, attack: 0.5, pitch: rrand(-0.2, 0.3)
            sleep 2
  end
end

in_thread delay: 90 do
 while Time.new - start_time < 102 do
                    sample :bd_fat
                    sleep 0.5
  end
end

#This should look familiar to those who pay attention. :P
in_thread delay: 102 do
 while Time.new - start_time < 135 do
    sample :drum_snare_soft, amp: 1.5 if (spread 3, 8).tick
    sample :drum_cymbal_soft, amp: 0.8 if (spread 7, 11).look
    sample :bd_fat, amp: 2 if (spread 1, 4).look
    sleep 0.125
  end
end