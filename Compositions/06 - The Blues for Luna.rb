# Charlie K - 2016
# 06 - The Blues for Luna

# In loving memory of Luna, me and my mothers cat (I took care of him a lot)
# who has died at the age of nine due to several illnesses at Friday, 20 may 2016.
# He had to be put to sleep.
# I Love You, Luna. I hope you'll find your way to heaven and enjoy.

# This is a work in progress!
# So it won't be finished just yet, but you'll get the general idea of what it'll be.

set_volume! 3
end_time = vt
use_bpm 60

damp = 1.5 # Drumbeat amp
gamp = 1   # Guitar amp
grel = 3   # Guitar Release
bamp = 0.8 # Bass/Piano amp


# Using full guitar barre chords over Piano chords
guit_chord_e1_minor = (ring 40, 47, 52, 55, 59, 64)
guit_chord_a_minor = (ring 45, 52, 57, 60, 64, 69)
guit_chord_b_minor = (ring 47, 54, 59, 62, 66, 71)

in_thread(name: :beat) do
  while vt - end_time < 300 do
      sample :loop_breakbeat, amp: damp, rate: 0.75, lpf: 100, pitch: rrand(0, 0.5)
      sleep 2.5
    end
  end
  
  wait 5
  
  in_thread(name: :guitarchords) do
    use_synth :pluck
    with_fx :echo, decay: 3, phase: 0.25, mix: 0.99 do
      with_fx :flanger do
        while vt - end_time < 305 do
            
            2.times do
              play_chord guit_chord_e1_minor, release: grel, amp: gamp
              sleep 5
            end
            1.times do
              play_chord guit_chord_a_minor, release: grel, amp: gamp
              sleep 5
            end
            1.times do
              play_chord guit_chord_e1_minor, release: grel, amp: gamp
              sleep 5
            end
            1.times do
              play_chord guit_chord_b_minor, release: grel, amp: gamp
              sleep 2.5
            end
            1.times do
              play_chord guit_chord_a_minor, release: grel, amp: gamp
              sleep 2.5
            end
            1.times do
              play_chord guit_chord_e1_minor, release: grel, amp: gamp
              sleep 2.5
            end
            1.times do
              play_chord guit_chord_b_minor, release: grel, amp: gamp
              sleep 2.5
            end
          end
        end
      end
    end
    
    use_synth :piano
    with_fx :reverb, room: 1, damp: 0.9, mix: 0.2 do
      with_fx :flanger do
        
        play_pattern_timed [:g3],[1]
        play_pattern_timed [:fs3, :e3],[0.25]
        sleep 2
        play_pattern_timed [:fs3, :g3, :a3],[0.25]
        sleep 0.5
        play_pattern_timed [:fs3, :e3],[0.25]
        sleep 4.75
        play_pattern_timed [:a3, :g3, :a3, :g3, :a3, :g3, :a3, :g3, :a3, :g3, :a3, :g3],[0.125]
        sleep 1.5
        play_pattern_timed [:a3, :g3, :a3, :c4, :b3, :a3, :g3, :fs3, :e3],[0.25]
        
        
        
      end
    end
    