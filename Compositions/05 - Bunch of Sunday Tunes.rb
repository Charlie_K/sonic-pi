# Charlie K - 2016

# This is basically my way of learning.
# I look at what others do and replicate it to understand and master it.
# You will find stuff here based on what the following people have done.

# based/copied on/from Sam Aaron at:
# https://www.livecoding.tv/samaaron/

# The seeds I took from Nanomancer at:
# https://github.com/Nanomancer/sonic-pi-projects/blob/master/eastern_ambient.rb

# Sam Aaron and Nanomancer. Credits to them!

# Personal Note:
# SonicPi is a musical instrument making it fun to learn making music.
# Let's all teach each other. :)
# http://sonic-pi.net/

claps = "d:/_samples/ghost_1/Claps/"
set_volume! 2

seeds = [323699.5012207031, 323705.5012207031, 323723.5012207031,
         323729.5012207031, 323735.5012207031, 323747.5012207031,
         323759.5012207031, 323771.5012207031, 323831.5012207031,
         323837.5012207031, 323861.5012207031, 323867.5012207031,
         323897.5012207031, 323993.5012207031, 324017.5012207031,
         323729.5012207031, 323755.5012207031, 323677.5012207031,
         323761.5012207031].ring

notes1 = (scale :e1, :hirajoshi, num_octaves: 5)
notes2 = (scale :a1, :hex_major7, num_octaves: 5)
notes3 = (scale :b1, :diminished, num_octaves: 4)
notes4 = (scale :g1, :prometheus, num_octaves: 4)
notes5 = (scale :c1, :minor_pentatonic, num_octaves: 4)
notes6 = (scale :d1, :super_locrian, num_octaves: 5, rand: 5)
notes7 = (scale :f1, :hungarian_minor, num_octaves: 4)

# Can NOT use more than ONE #hashtag !!!
# This goes for both USE_SYNTH as well as PLAY !!
# Keep in mind that some synths are LOUDER than the other, play with amp
live_loop :tunes do |idx|
  #use_synth :growl
  #use_synth :dark_ambience
  use_synth :prophet
  #use_synth :beep
  use_synth_defaults release: 0.1, cutoff: 100, amp: 2, lpf: 80
  with_fx :reverb, room: 1, reps: 32 do
    with_fx :bitcrusher, cutoff: 120 do
      with_fx :none do
        with_random_seed seeds[idx] do
          puts "SEED: #{current_random_seed} loop no.: #{idx}"
          if idx == 3 then stop end
          
          # Below the amount of times for the loops
          # Can only have one enabled, hashtag the rest out
          #32.times do
          16.times do
            #8.times do
            # 4.times do
            
            # Here are the notes we've created
            # Can only have one enabled, hashtag the rest out
            #play notes1.choose
            #play notes2.choose
            #play notes3.choose
            #play notes4.choose
            #play notes5.choose
            #play notes6.choose
            #play notes7.choose
            sleep 0.125
          end
        end
      end
    end
  end
end

live_loop :guitar1 do
  #sample :guit_em9, pitch: rrand(-1, 1), sustain: 10, release: 10, rate: rrand(-2, 1)
  sleep 10
end

# All below CAN use MORE than one #hashtag!! ##
live_loop :chill, delay: 4 do
  with_fx :bitcrusher, cutoff: 100, sample_rate: 3000022 do
    #sample :ambi_soft_buzz, rpitch: 0 - 3, amp: 1, pitch: rrand(1, 2)
    #sample :ambi_swoosh, rpitch: 3 - 8, pitch: rrand(1, 2), amp: 1
    #sample :ambi_glass_hum, rpitch: 5 - 3, amp: 1, pitch: rrand(-5, 2)
    #sample :ambi_glass_rub, rate: rrand(-1, 0.5), beat_stretch: 8, amp: 1, pitch: rrand(-1, 1), release: 8
    #sample :bass_trance_c, rpitch: 5 - 3, pitch: rrand(-1, 0.8), amp: 1, beat_stretch: 7, sustain: 7, attack: 4
    #sample :ambi_dark_woosh, rpitch: 2 - 4, amp: 1, pitch: rrand(0.1, 1.4)
    #sample :perc_bell, rate: rrand(-1, 0.5), beat_stretch: 8, amp: 1, pitch: rrand(-1, 1), release: 8
    #sample :ambi_lunar_land, rate: rrand(-1, 0.5), beat_stretch: 8, amp: 2, pitch: rrand(-1, 1), release: 8
  end
  sleep 8
end

# The two loops below are beats
live_loop :beat1 do
  #sample :bd_haus, amp: 1, lpf: 120 if (spread 2, 4).tick
  #sample :drum_bass_hard, amp: 1, lpf: 120 if (spread 2, 4).tick
  #sample :bd_gas, amp: 2, lpf: 100 if (spread 2, 4).tick
  #sample :bd_sone, amp: 1 if (spread 2, 4).tick
  #sample claps, 5, lpf: 100 if (spread 1, 4).tick
  sleep 0.250
end

live_loop :snareclap do
  sync :beat1
  #sample :drum_snare_soft, amp: 1, lpf: 120 if (spread 1, 4).tick
  #sample :drum_snare_hard, amp: 0.7, lpf: 120 if (spread 1, 4).tick
  #sample claps, 0, amp: 1, lpf: 120 if (spread 1, 4).tick
  sleep 0.250
end


# [Illusionist] Ocean example
live_loop :oceans do
  with_fx :reverb, mix: 0.5 do
    #s = synth [:bnoise, :cnoise, :gnoise].choose, attack: rrand(0, 4), sustain: rrand(0, 2), release: rrand(1, 5), cutoff_slide: rrand(0, 5), cutoff: rrand(60, 100), pan: rrand(-1, 1), pan_slide: rrand(1, 5), amp: rrand(0.5, 1)
    #control s, pan: rrand(-1, 1), cutoff: rrand(60, 110), amp: rrand(0.1, 0.4)
    sleep rrand(3, 4)
  end
end
