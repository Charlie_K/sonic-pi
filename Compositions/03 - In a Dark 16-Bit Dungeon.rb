# Charlie K - 2016
# In a Dark 16-bit Dungeon

print "Charlie K was Here"
print "In a Dark 16-bit Dungeon"
start_time = vt
end_time = vt

in_thread do
  while vt - end_time < 84 do
      sample :bd_haus, cutoff: 80
      sleep 0.25
      sample :drum_bass_hard
      sample :drum_cymbal_closed
      sleep 0.25
    end
end

in_thread delay: 4 do
    while vt - end_time < 80 do
    sample :loop_industrial, beat_stretch: 2, amp: 0.6
    sleep 2
  end
end

in_thread delay: 8 do
  2.times do
    use_synth :mod_pulse
    use_synth_defaults amp: 1, mod_range: 10, cutoff: 100, pulse_width: 0.3, attack: 0.1, release: 0.5, mod_phase: 0.25, mod_invert_wave: 1, rate: -1
    play_pattern_timed [34, 34, 32, 32, 31, 31, 30, 30],[1]
  end
end

in_thread delay: 16 do
  8.times do
    use_synth :dpulse
    use_synth_defaults amp: 0.5, mod_range: 1, cutoff: 100, pulse_width: 0.4, attack: 0.03, release: 0.6, mod_phase: 0.50, mod_invert_wave: rrand(-1, 1), rate: rrand(-1, 1)
    play_pattern_timed [72, 68, 72, 68, 72, 68, 72, 68, 72, 66, 72, 66, 72, 66, 72, 66, 72, 65, 72, 65, 72, 65, 72, 65, 72, 64, 72, 64, 72, 64, 72, 64],[0.25]
  end
end
in_thread delay: 24 do
  8.times do
    use_synth :dpulse
    use_synth_defaults amp: 0.6, mod_range: 10, cutoff: 100, pulse_width: 0.1, attack: 0.03, release: 0.6, mod_phase: 0.25, mod_invert_wave: 1, rate: -1
    play_pattern_timed [60, 60, 60, 60, 60, 61, 63],[0.25]
    sleep 0.25
  end
end

in_thread delay: 40 do
  3.times do
    use_synth :dpulse
    use_synth_defaults amp: 0.6, release: 0.8, attack: 0.2
    play_pattern_timed [60],[1]
    play_pattern_timed [61, 63, 66],[0.5]
    play_pattern_timed [68],[1]
    play_pattern_timed [66],[0.5]
    play_pattern_timed [68],[4]
  end
end

in_thread delay: 64 do  
  1.times do
    use_synth :dpulse
    use_synth_defaults amp: 0.6, release: 0.8, attack: 0.2
    play_pattern_timed [60],[1]
    play_pattern_timed [61, 63],[0.5]
    play_pattern_timed [58],[1]
    play_pattern_timed [61, 63],[0.5]
    play_pattern_timed [68, 67],[0.25]
    play_pattern_timed [68],[3.5]
    play_pattern_timed [60],[1]
    play_pattern_timed [61, 63],[0.5]
    play_pattern_timed [58],[1]
    play_pattern_timed [61, 63],[0.5]
    play_pattern_timed [68, 67],[0.25]
    play_pattern_timed [68],[1]
    play_pattern_timed [70],[0.5]
    play_pattern_timed [68],[2.5]  
  end
end

# Ocean sound was Coded by Sam Aaron
# And used as an example.
# Sam, this is too good to ignore! Thank You!
with_fx :reverb, mix: 0.5, amp: rrand(0.0, 0.2) do
          print  "Exit of the Dungeon and onto the beach"
  in_thread delay: 82 do
    20.times do
    s = synth [:bnoise, :cnoise, :gnoise].choose, attack: rrand(0, 4), sustain: rrand(0, 2), release: rrand(1, 5), cutoff_slide: rrand(0, 5), cutoff: rrand(60, 100), pan: rrand(-1, 1), pan_slide: rrand(1, 5), amp: rrand(0.5, 1)
    control s, pan: rrand(-1, 1), cutoff: rrand(60, 110)
    sleep rrand(2, 4)
        end
    end
end

in_thread delay: 90 do
  2.times do
    use_synth :piano
    use_synth_defaults amp: 1, release: 0.8, attack: 0.2
    play_pattern_timed [60],[1]
    play_pattern_timed [61, 63],[0.5]
    play_pattern_timed [58],[1]
    play_pattern_timed [61, 63],[0.5]
    play_pattern_timed [68, 67],[0.25]
    play_pattern_timed [68],[3.5]
    play_pattern_timed [60],[1]
    play_pattern_timed [61, 63],[0.5]
    play_pattern_timed [58],[1]
    play_pattern_timed [61, 63],[0.5]
    play_pattern_timed [68, 67],[0.25]
    play_pattern_timed [68],[1]
    play_pattern_timed [70],[0.5]
    play_pattern_timed [68],[2.5]
   end
end
