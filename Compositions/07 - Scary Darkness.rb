# Charlie K - 2016

samps = "d:/_samples/_self/"
load_samples samps

set_mixer_control! amp: 2

# Whispers
live_loop :whispers do
  with_fx :echo, room: 1 do
    with_fx :panslicer, phase: 0.9, wave: 3 do
      sample samps, 7, rate: rrand(0.5, -0.1), amp: 2
      sleep 16
    end
  end
end

# bassdrop
wait 20
live_loop :bassdrop do
  use_sample_defaults release: 0.4, rate: rrand(-0.5, 0.5), beat_stretch: 2, cutoff: 80
  sample :bass_drop_c
  sleep 4
end

# scary sounds
wait 5
live_loop :scarymix do
  sample :perc_bell, rate: rrand(-1, 0.3), amp: 0.1, sustain: 6, relax_time: 0.5, release: 5
  sample :bass_trance_c, rate: rrand(-2, 1), sustain: 6, relax_time: 0.5, release: 5
  sample :ambi_piano, rate: rrand(-0.1, 0.9), sustain: 6, relax_time: 0.5, release: 5
  sample :elec_twang, rate: rrand(-0.3, 0.9), sustain: 6, relax_time: 0.5, release: 5
  sleep 5
end

use_synth :hollow
with_fx :reverb, mix: 0.7 do
  with_fx :flanger, depth: 3, decay: 3, phase_offset: 1 do
    
    live_loop :note1 do
      play choose([:Ds2,:E2]), attack: 6, release: 6, amp: 2
      sleep 8
    end
    
    live_loop :note2 do
      play choose([:Cs2,:D2]), attack: 4, release: 5, amp: 2
      sleep 10
    end
    
    live_loop :note3 do
      play choose([:As2, :Cs2]), attack: 5, release: 5, amp: 2
      sleep 12
    end
    
    live_loop :note4 do
      play choose([:G2, :As2]), attack: 5, release: 5, amp: 2
      sleep 14
    end
    
    live_loop :note5 do
      play choose([:F2, :G2]), attack: 5, release: 5, amp: 2
      sleep 16
    end
    
    live_loop :note6 do
      play choose([:E2, :Fs2]), attack: 5, release: 5, amp: 2
      sleep 18
    end
  end
end
