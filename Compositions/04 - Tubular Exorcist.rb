# Charlie K - 2016
# 04 - Tubular Exorcist

# Thanks to many FREE Samples by
#      GHOSTHACK.DE

puts "Charlie K Was Here"
puts "04 - Tubular Exorcist"
# Bassdrums
bassdrum1 = "d:/_samples/ghost_1/Bassdrums/"
bassdrum2 = "d:/_samples/ghost_5/Bassdrums/"
# Basslines
bassline1 = "d:/_samples/ghost_1/Basslines/"
bassline2 = "d:/_samples/ghost_4/Basslines/"
# FX
fx1 = "d:/_samples/ghost_1/FX/"
fx2 = "d:/_samples/ghost_2/FX/"
# Drum Loops
loop1 = "d:/_samples/ghost_1/Loops/"
# Standards
self1 = "d:/_samples/_self"
# Standards
end_time = vt
set_mixer_control! amp: 2
amp_m = (range 0.2, 1, step: 0.1).ramp
amp_f = 1


in_thread delay: 0 do
 while vt - end_time < 22 do
    use_synth :piano
    with_fx :reverb do
    use_synth_defaults amp: 1, sustain: 0.1, release: 0.2, cutoff: 100
    play_pattern_timed [:A5, :E5, :B5, :E5,:G5, :A5, :E5, :C6, :E5, :D6, :E5, :B5, :C6, :E5, :A5, :E5, :B5, :E5, :G5, :A5, :E5, :C6, :E5, :D6, :E5, :B5, :C6, :E5],[0.2]
      end
  end
end

in_thread delay: 4 do
  while vt - end_time < 76 do
      sample fx1, 11, release: 0.3, pitch: rrand(0.5, 1), amp: 0.8
    sleep 16
    end
end

in_thread delay: 8 do
 while vt - end_time < 24 do
        sample bassdrum1, 1, amp: 2
            sleep 0.4
  end
end

in_thread delay: 8 do
 while vt - end_time < 60 do
          sample fx2, 5, amp: 1, release: 0.8, attack: 0.2, pitch: rrand(-0.3, 0.5)
          sleep rrand(1, 9)
    end
end

in_thread delay: 16 do
 while vt - end_time < 70 do
            sample bassline1, 10, amp: 2, release: 2, attack: 0.5, pitch: rrand(-0.3, 0.5)
            sleep 2
  end
end

in_thread delay: 22 do
 while vt - end_time < 90 do
        sample loop1, 1, amp: 2
        sleep 6
    end
end

in_thread delay: 96 do
 while vt - end_time < 126 do
        sample bassdrum2, 0, amp: 2
            sleep 0.4
  end
end

in_thread delay: 104 do
1.times do
        sample self1, 0, amp: 2
        sleep 19
    end
end

in_thread delay: 116 do
 while vt - end_time < 126 do
    use_synth :supersaw
    with_fx :reverb do
        if (spread 1, 1).tick then
    use_synth_defaults amp: amp_m.look * amp_f, sustain: 0.1, release: 0.2, cutoff: 100, pitch: -12
    play_pattern_timed [:A5, :E5, :B5, :E5,:G5, :A5, :E5, :C6, :E5, :D6, :E5, :B5, :C6, :E5, :A5, :E5, :B5, :E5, :G5, :A5, :E5, :C6, :E5, :D6, :E5, :B5, :C6, :E5],[0.2]
      end
  end
end
end

in_thread delay: 126 do
 while vt - end_time < 174 do
        sample loop1, 1, amp: 2
        sleep 6
    end
end

in_thread delay: 130 do
  while vt - end_time < 200 do
      sample fx1, 11, release: 0.3, pitch: rrand(0.5, 1), amp: 0.8
    sleep 16
    end
end

in_thread delay: 134 do
 while vt - end_time < 200 do
          sample fx2, 5, amp: 1, release: 0.8, attack: 0.2, pitch: rrand(-0.3, 0.5)
          sleep rrand(1, 9)
    end
end

in_thread delay: 180 do
 while vt - end_time < 200 do
          sample self1, 3, amp: 1, release: 0.8, attack: 0.2
          sleep 35
    end
end
