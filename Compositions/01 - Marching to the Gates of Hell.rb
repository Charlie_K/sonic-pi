# Charlie K - 2016
# Welcome to Hell

samps = "d:/_samples/_self/"
load_samples samps

start_time = Time.new
loop_end_time = Time.new
duration = loop_end_time - start_time
set_mixer_control! amp: 2

puts "Charlie K Was Here"
puts "start_time: #{start_time}"
puts "end_time: #{loop_end_time}"
puts "duration: #{duration}"


# Military march to the gates of Hell
in_thread delay: 0 do
while Time.new - start_time < 41 do
      sample samps, 0, amp: 0.4
      sleep 21
    end
  end

# Whispers
in_thread delay: 2 do
    with_fx :echo, room: 10 do
      while Time.new - start_time < 180 do
        with_fx :panslicer, phase: 0.9, wave: 3 do
          sample samps, 1, rate: 0.7
          sleep 16
        end
      end
    end
  end

in_thread delay: 4 do
while Time.new - start_time < 178 do
    use_sample_defaults release: 0.4, rate: rrand(-0.5, 0.5), beat_stretch: 2, cutoff: 80
    sample :bass_drop_c
    sleep 4
  end
end

in_thread delay: 8 do
while Time.new - start_time < 180 do
    sample :perc_bell, rate: rrand(-1, 0.3), amp: 0.3
    sample :bass_trance_c, rate: rrand(-0.5, 1)
    sample :ambi_piano, rate: rrand(-0.1, 0.9)
    sample :elec_twang, rate: rrand(-0.3, 0.9)
    sleep 4
  end
end