# This is a direct copy, with some additions, of Sam Aaron's 
# ---> TILBURG 2 <---
# I've been playing with this kind of in addition to an earlier one where I called it
# Bunch of Sunday Sounds

# I am not really a good algorithmic coder so I am allowing myself to promote 
#                                SonicPi
# By playing with the samples and every now and then make a structured something.

# Recorded at Sunday 20th of November 2016
# Using SonicPi V 2.11
# http://sonic-pi.net/

use_debug false
load_samples :bd_haus

live_loop :low do
  tick
  #synth :zawa, wave: 1, phase: 0.5, release: 5, note: (knit :e1, 12, :e2, 4).look, cutoff: (line 60, 120, steps: 6).look
  #synth :zawa, wave: 1, phase: 0.5, release: 5, note: (knit :a1, 12, :a2, 4).look, cutoff: (line 60, 120, steps: 6).look
  sleep 4
end

with_fx :reverb, room: 1 do
  live_loop :lands, auto_cue: false do
    use_synth :dsaw
    use_random_seed 254126
    E = (scale :e2, :minor_pentatonic, num_octaves: 4).take(4)
    A = (scale :a2, :minor_pentatonic, num_octaves: 4).take(4)
    B = (scale :b2, :minor_pentatonic, num_octaves: 4).take(4)
    16.times do
      #play E.choose, detune: 12, release: 0.1, amp: 2, amp: rand + 0.5, cutoff: rrand(70, 120), amp: 2
      #play A.choose, detune: 12, release: 0.1, amp: 2, amp: rand + 0.5, cutoff: rrand(70, 120), amp: 2
      sleep 0.125
    end
  end
end

# All below CAN use MORE than one #hashtag!! ##
live_loop :chill, delay: 4 do
  with_fx :bitcrusher, cutoff: 100, sample_rate: 3000022 do
    #sample :ambi_soft_buzz, rpitch: 0 - 3, amp: 1, pitch: rrand(1, 2)
    #sample :ambi_swoosh, rpitch: 3 - 8, pitch: rrand(1, 2), amp: 1
    #sample :ambi_glass_hum, rpitch: 5 - 3, amp: 1, pitch: rrand(-5, 2)
    #sample :ambi_glass_rub, rate: rrand(-1, 0.5), beat_stretch: 8, amp: 2, pitch: rrand(-1, 1), release: 8
    #sample :bass_trance_c, rpitch: 5 - 3, pitch: rrand(-1, 0.8), amp: 3, beat_stretch: 7, sustain: 7, attack: 4
    #sample :ambi_dark_woosh, rpitch: 2 - 4, amp: 3, pitch: rrand(0.1, 1.4)
    #sample :perc_bell, rate: rrand(-1, 0.5), beat_stretch: 8, amp: 1, pitch: rrand(-1, 1), release: 8
    sample :ambi_lunar_land, rate: rrand(-1, 0.5), beat_stretch: 8, amp: 3, pitch: rrand(-1, 1), release: 8
    #sample :guit_em9, pitch: rrand(-1, 1), sustain: 10, release: 10, rate: rrand(-2, 1)
    #sample :guit_harmonics, pitch: rrand(-1, 1), sustain: 10, release: 10, rate: rrand(-2, 1)
  end
  sleep 8
end

live_loop :tijd do
  #sample :bd_haus, amp: 2.5, cutoff: 100
  sleep 0.5
end

live_loop :ind do
  #sample :loop_industrial, beat_stretch: 1
  sleep 1
end