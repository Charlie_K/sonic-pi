use_bpm 40


live_loop :light_drums do
  1. times do
    with_fx :reverb, room: 1, damp: 0.7 do
      sample :drum_tom_lo_hard, rate: rrand(1, 0.2), pitch_dis: 0.001
      sleep 0.75
      sample :drum_tom_lo_hard, rate: rrand(1, 0.2), pitch_dis: 0.001
      sleep 2
    end
    sleep 0.75
  end
end