#Fade Out
master = (range 1.2, 0.2, step: 0.2).ramp
a_drone = 1.2

# Nice dark Drone.

live_loop :Drone do
  with_synth :hollow do
    with_fx :flanger, mix: 0.3 do
      with_fx :reverb, room: 1, mix: 0.8 do
        if (spread 1, 1).tick then
          play :g0, amp: master.look * a_drone, sustain: 3
        end
        sleep 3
      end
    end
  end
end