1.times do
  use_synth :blade
  with_fx :bitcrusher do
    a = play :c4, sustain: 8, note_slide_shape: 2, note_slide_curve: 2
    b = play :g5, sustain: 8, note_slide_shape: 2, note_slide_curve: 1
    control a, note: :c5, note_slide: 4
    control b, note: :g4, note_slide: 4
  end
end
