## By Charlie K - 2016

In these directories you'll find several SonicPi scripts I wrote for the public to use.

Currently I am working on adding a library of guitar chords.
Not only for myself is this handy in future SonicPi projects,
	but I also wish others will pick it up to aid in teaching/learning the guitar.
	
So feel free to use it all but remember: Sharing = Caring!

Use all well to learn and teach others once you're done.