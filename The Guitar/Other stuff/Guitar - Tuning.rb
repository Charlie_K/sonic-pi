# By Charlie K - 2016

# A standard simple aide to tune the guitar
# In Sonic Pi these notes are the following:

# E:64, B:59, G:55, D:50, A:45, E:40
# OR
# :e4, :b3, :g3, :d3, :a2, :e2

set_volume! 3
use_synth :pluck

# Just the notes as they are in a quicker way
play_pattern_timed [:e2, :a2, :d3, :g3, :b3, :e4],[1]
sleep 2

# 6th string E
5.times do
  play :e2
  sleep 2
end

# 5th string A
5.times do
  play :a2
  sleep 2
end

# 4th string D
5.times do
  play :d3
  sleep 2
end

# 3rd string G
5.times do
  play :g3
  sleep 2
end

# 2nd string B
5.times do
  play :b3
  sleep 2
end

# 1st string E
5.times do
  play :e4
  sleep 2
end

# Tune along
sleep 2
play_pattern_timed [:e2, :a2, :d3, :g3, :b3, :e4],[0.5]
