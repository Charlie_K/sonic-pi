# By Charlie K - 2016
# This is my contribution to those wishing to use guitar chords,
# or teach/learn to play the guitar by sound.

# These are the basic 7th guitar chords we all learn at first.

# Personal Note:
# E:64, B:59, G:55, D:50, A:45, E:40

guit_chord_e_7 = (ring 40, 47, 50, 56, 59, 64)
guit_chord_a_7 = (ring 45, 52, 57, 61, 67)
guit_chord_b_7 = (ring 47, 51, 57, 59, 66)
guit_chord_c_7 = (ring 48, 52, 58, 60, 64)
guit_chord_d_7 = (ring 50, 57, 59, 66)
guit_chord_f_7 = (ring 41, 48, 51, 57, 60, 65)
guit_chord_g_7 = (ring 43, 47, 50, 55, 59, 65)

use_synth :pluck

# E - Chord
play guit_chord_e_7
sleep 1
play_pattern_timed guit_chord_e_7,[0.25]
sleep 1

# A - Chord
play guit_chord_a_7
sleep 1
play_pattern_timed guit_chord_a_7,[0.25]
sleep 1

# B - Chord
play guit_chord_b_7
sleep 1
play_pattern_timed guit_chord_b_7,[0.25]
sleep 1

# C - Chord
play guit_chord_c_7
sleep 1
play_pattern_timed guit_chord_c_7,[0.25]
sleep 1

# D - Chord
play guit_chord_d_7
sleep 1
play_pattern_timed guit_chord_d_7,[0.25]
sleep 1

# F - Chord
play guit_chord_f_7
sleep 1
play_pattern_timed guit_chord_f_7,[0.25]
sleep 1

# G - Chord
play guit_chord_g_7
sleep 1
play_pattern_timed guit_chord_g_7,[0.25]
sleep 1