# By Charlie K - 2016
# This is my contribution to those wishing to use guitar chords,
# or teach/learn to play the guitar by sound.

# These are the basic BARRE major guitar chords
# e1_major is the chord where Index, Middle and Ring fingers are on B2, E3 and Gs3

guit_chord_e1_major = (ring 40, 47, 52, 56, 59, 64)
guit_chord_f_major = (ring 41, 48, 53, 57, 60, 65)
guit_chord_fs_major = (ring 42, 49, 54, 58, 61, 66)
guit_chord_g_major = (ring 43, 50, 55, 59, 62, 67)
guit_chord_gs_major = (ring 44, 51, 56, 60, 63, 68)
guit_chord_a_major = (ring 45, 52, 57, 61, 64, 69)
guit_chord_as_major = (ring 46, 53, 58, 62, 65, 70)
guit_chord_b_major = (ring 47, 54, 59, 63, 66, 71)
guit_chord_c_major = (ring 48, 55, 60, 64, 67, 72)
guit_chord_cs_major = (ring 49, 56, 61, 65, 68, 73)
guit_chord_d_major = (ring 50, 57, 62, 66, 69, 74)
guit_chord_ds_major = (ring 51, 58, 63, 67, 70, 75)
guit_chord_e2_major = (ring 52, 59, 64, 68, 71, 76)


use_synth :pluck
play guit_chord_e1_major
sleep 1
play_pattern_timed guit_chord_e1_major,[0.25]
sleep 1
play guit_chord_f_major
sleep 1
play_pattern_timed guit_chord_f_major,[0.25]
sleep 1
play guit_chord_fs_major
sleep 1
play_pattern_timed guit_chord_fs_major,[0.25]
sleep 1
play guit_chord_g_major
sleep 1
play_pattern_timed guit_chord_g_major,[0.25]
sleep 1
play guit_chord_gs_major
sleep 1
play_pattern_timed guit_chord_gs_major,[0.25]
sleep 1
play guit_chord_a_major
sleep 1
play_pattern_timed guit_chord_a_major,[0.25]
sleep 1
play guit_chord_as_major
sleep 1
play_pattern_timed guit_chord_as_major,[0.25]
sleep 1
play guit_chord_b_major
sleep 1
play_pattern_timed guit_chord_b_major,[0.25]
sleep 1
play guit_chord_c_major
sleep 1
play_pattern_timed guit_chord_c_major,[0.25]
sleep 1
play guit_chord_cs_major
sleep 1
play_pattern_timed guit_chord_cs_major,[0.25]
sleep 1
play guit_chord_d_major
sleep 1
play_pattern_timed guit_chord_d_major,[0.25]
sleep 1
play guit_chord_ds_major
sleep 1
play_pattern_timed guit_chord_ds_major,[0.25]
sleep 1
play guit_chord_e2_major
sleep 1
play_pattern_timed guit_chord_e2_major,[0.25]
sleep 1