# By Charlie K - 2016
# This is my contribution to those wishing to use guitar chords,
# or teach/learn to play the guitar by sound.

# These are the basic E Shaped barre minor guitar chords
# e1_minor is an open chord with just the middle and ring finger on B2 and E3

guit_chord_e1_minor = (ring 40, 47, 52, 55, 59, 64)
guit_chord_f_minor = (ring 41, 48, 53, 56, 60, 65)
guit_chord_fs_minor = (ring 42, 49, 54, 57, 61, 66)
guit_chord_g_minor = (ring 43, 50, 55, 58, 62, 67)
guit_chord_gs_minor = (ring 44, 51, 56, 59, 63, 68)
guit_chord_a_minor = (ring 45, 52, 57, 60, 64, 69)
guit_chord_as_minor = (ring 46, 53, 58, 61, 65, 70)
guit_chord_b_minor = (ring 47, 54, 59, 62, 66, 71)
guit_chord_c_minor = (ring 48, 55, 60, 63, 67, 72)
guit_chord_cs_minor = (ring 49, 56, 61, 64, 68, 73)
guit_chord_d_minor = (ring 50, 57, 62, 65, 69, 74)
guit_chord_ds_minor = (ring 51, 58, 63, 66, 70, 75)
guit_chord_e2_minor = (ring 52, 59, 64, 67, 71, 76)


use_synth :pluck
play guit_chord_e1_minor
sleep 1
play_pattern_timed guit_chord_e1_minor,[0.25]
sleep 1
play guit_chord_f_minor
sleep 1
play_pattern_timed guit_chord_f_minor,[0.25]
sleep 1
play guit_chord_fs_minor
sleep 1
play_pattern_timed guit_chord_fs_minor,[0.25]
sleep 1
play guit_chord_g_minor
sleep 1
play_pattern_timed guit_chord_g_minor,[0.25]
sleep 1
play guit_chord_gs_minor
sleep 1
play_pattern_timed guit_chord_gs_minor,[0.25]
sleep 1
play guit_chord_a_minor
sleep 1
play_pattern_timed guit_chord_a_minor,[0.25]
sleep 1
play guit_chord_as_minor
sleep 1
play_pattern_timed guit_chord_as_minor,[0.25]
sleep 1
play guit_chord_b_minor
sleep 1
play_pattern_timed guit_chord_b_minor,[0.25]
sleep 1
play guit_chord_c_minor
sleep 1
play_pattern_timed guit_chord_c_minor,[0.25]
sleep 1
play guit_chord_cs_minor
sleep 1
play_pattern_timed guit_chord_cs_minor,[0.25]
sleep 1
play guit_chord_d_minor
sleep 1
play_pattern_timed guit_chord_d_minor,[0.25]
sleep 1
play guit_chord_ds_minor
sleep 1
play_pattern_timed guit_chord_ds_minor,[0.25]
sleep 1
play guit_chord_e2_minor
sleep 1
play_pattern_timed guit_chord_e2_minor,[0.25]
sleep 1