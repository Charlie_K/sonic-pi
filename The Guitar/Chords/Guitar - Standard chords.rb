# By Charlie K - 2016
# This is my contribution to those wishing to use guitar chords,
# or teach/learn to play the guitar by sound.

# These are the basic guitar chords we all learn at first.

# Personal Note:
# E:64, B:59, G:55, D:50, A:45, E:40

guit_chord_e = (ring 40, 47, 52, 56, 59, 64)
guit_chord_a = (ring 45, 52, 57, 61, 64)
guit_chord_b = (ring 54, 59, 63, 66)
guit_chord_c = (ring 48, 52, 55, 60, 64)
guit_chord_d = (ring 50, 57, 62, 66)
guit_chord_f = (ring 53, 57, 60, 65)
guit_chord_g = (ring 43, 47, 50, 55, 59, 67)

use_synth :pluck

# E - Chord
play guit_chord_e
sleep 1
play_pattern_timed guit_chord_e,[0.25]
sleep 1

# A - Chord
play guit_chord_a
sleep 1
play_pattern_timed guit_chord_a,[0.25]
sleep 1

# B - Chord
play guit_chord_b
sleep 1
play_pattern_timed guit_chord_b,[0.25]
sleep 1

# C - Chord
play guit_chord_c
sleep 1
play_pattern_timed guit_chord_c,[0.25]
sleep 1

# D - Chord
play guit_chord_d
sleep 1
play_pattern_timed guit_chord_d,[0.25]
sleep 1

# F - Chord
play guit_chord_f
sleep 1
play_pattern_timed guit_chord_f,[0.25]
sleep 1

# G - Chord
play guit_chord_g
sleep 1
play_pattern_timed guit_chord_g,[0.25]
sleep 1