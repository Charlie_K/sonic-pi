# By Charlie K - 2016
# This is my contribution to those wishing to use guitar chords,
# or teach/learn to play the guitar by sound.

# These are the basic _MINOR_ guitar chords we all learn at first.

# Personal Note:
# E:64, B:59, G:55, D:50, A:45, E:40

guit_chord_e_minor = (ring 40, 47, 52, 55, 59, 64)
guit_chord_a_minor = (ring 45, 52, 57, 60, 64)
guit_chord_b_minor = (ring 54, 59, 62, 66)
guit_chord_c_minor = (ring 48, 51, 55, 60)
guit_chord_d_minor = (ring 50, 57, 62, 65)
guit_chord_f_minor = (ring 53, 56, 60, 65)
guit_chord_g_minor = (ring 58, 62, 67)

use_synth :pluck

# E - Chord
play guit_chord_e_minor
sleep 1
play_pattern_timed guit_chord_e_minor,[0.25]
sleep 1

# A - Chord
play guit_chord_a_minor
sleep 1
play_pattern_timed guit_chord_a_minor,[0.25]
sleep 1

# B - Chord
play guit_chord_b_minor
sleep 1
play_pattern_timed guit_chord_b_minor,[0.25]
sleep 1

# C - Chord
play guit_chord_c_minor
sleep 1
play_pattern_timed guit_chord_c_minor,[0.25]
sleep 1

# D - Chord
play guit_chord_d_minor
sleep 1
play_pattern_timed guit_chord_d_minor,[0.25]
sleep 1

# F - Chord
play guit_chord_f_minor
sleep 1
play_pattern_timed guit_chord_f_minor,[0.25]
sleep 1

# G - Chord
play guit_chord_g_minor
sleep 1
play_pattern_timed guit_chord_g_minor,[0.25]
sleep 1