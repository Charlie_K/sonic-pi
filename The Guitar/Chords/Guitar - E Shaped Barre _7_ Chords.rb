# By Charlie K - 2016
# This is my contribution to those wishing to use guitar chords,
# or teach/learn to play the guitar by sound.

# These are the basic E Shaped barre 7th guitar chords
# e1_7 is an open chord with just the middle and ring finger on B2 and gs3

guit_chord_e1_7 = (ring 40, 47, 50, 55, 59, 64)
guit_chord_f_7 = (ring 41, 48, 51, 56, 60, 65)
guit_chord_fs_7 = (ring 42, 49, 52, 57, 61, 66)
guit_chord_g_7 = (ring 43, 50, 53, 58, 62, 67)
guit_chord_gs_7 = (ring 44, 51, 54, 59, 63, 68)
guit_chord_a_7 = (ring 45, 52, 55, 60, 64, 69)
guit_chord_as_7 = (ring 46, 53, 56, 61, 65, 70)
guit_chord_b_7 = (ring 47, 54, 57, 62, 66, 71)
guit_chord_c_7 = (ring 48, 55, 58, 63, 67, 72)
guit_chord_cs_7 = (ring 49, 56, 59, 64, 68, 73)
guit_chord_d_7 = (ring 50, 57, 60, 65, 69, 74)
guit_chord_ds_7 = (ring 51, 58, 61, 66, 70, 75)
guit_chord_e2_7 = (ring 52, 59, 62, 67, 71, 76)


use_synth :pluck
play guit_chord_e1_7
sleep 1
play_pattern_timed guit_chord_e1_7,[0.25]
sleep 1
play guit_chord_f_7
sleep 1
play_pattern_timed guit_chord_f_7,[0.25]
sleep 1
play guit_chord_fs_7
sleep 1
play_pattern_timed guit_chord_fs_7,[0.25]
sleep 1
play guit_chord_g_7
sleep 1
play_pattern_timed guit_chord_g_7,[0.25]
sleep 1
play guit_chord_gs_7
sleep 1
play_pattern_timed guit_chord_gs_7,[0.25]
sleep 1
play guit_chord_a_7
sleep 1
play_pattern_timed guit_chord_a_7,[0.25]
sleep 1
play guit_chord_as_7
sleep 1
play_pattern_timed guit_chord_as_7,[0.25]
sleep 1
play guit_chord_b_7
sleep 1
play_pattern_timed guit_chord_b_7,[0.25]
sleep 1
play guit_chord_c_7
sleep 1
play_pattern_timed guit_chord_c_7,[0.25]
sleep 1
play guit_chord_cs_7
sleep 1
play_pattern_timed guit_chord_cs_7,[0.25]
sleep 1
play guit_chord_d_7
sleep 1
play_pattern_timed guit_chord_d_7,[0.25]
sleep 1
play guit_chord_ds_7
sleep 1
play_pattern_timed guit_chord_ds_7,[0.25]
sleep 1
play guit_chord_e2_7
sleep 1
play_pattern_timed guit_chord_e2_7,[0.25]
sleep 1